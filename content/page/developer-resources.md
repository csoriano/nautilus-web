+++
date = "2015-07-18T14:08:35+02:00"
draft = false
title = "Developer Resources"
+++
Example of code
```c
static void
menu_provider_init_callback (void)
{
    GList *providers;
    GList *l;

    providers = nautilus_module_get_extensions_for_type (NAUTILUS_TYPE_MENU_PROVIDER);

    for (l = providers; l != NULL; l = l->next)
    {
        NautilusMenuProvider *provider = NAUTILUS_MENU_PROVIDER (l->data);

        g_signal_connect_after (G_OBJECT (provider), "items-updated",
                                (GCallback) menu_provider_items_updated_handler,
                                NULL);
    }

    nautilus_module_extension_list_free (providers);
}
```
