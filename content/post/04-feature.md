+++
date = "2015-07-18T14:53:21+02:00"
draft = false
title = "Feel safe"
img = "safe.png"
weight = 3
orientation = "left"
+++
Safety with your data is the most important for Nautilus. Any risky operation shows a notification and allows to undo it
