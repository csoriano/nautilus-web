+++
date = "2015-07-18T14:08:29+02:00"
draft = false
title = "Simple menus"
img = "menus.png"
weight = 1
orientation = "right"
+++
We aim to find the balance between crafted features and user-centered usability while maintaining the simplicity.
